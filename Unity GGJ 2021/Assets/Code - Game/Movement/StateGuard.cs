﻿using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "StateGuard", menuName = "State Guard", order = 0)]
    public class StateGuard : ScriptableObject
    {
        public StateRelevance Grounded = StateRelevance.NotRelevant;
        public StateRelevance Falling = StateRelevance.NotRelevant;
        public StateRelevance Airborn = StateRelevance.NotRelevant;
        public StateRelevance Jumping = StateRelevance.NotRelevant;
        public StateRelevance OnWall = StateRelevance.NotRelevant;
        public StateRelevance OnLeftWall = StateRelevance.NotRelevant;
        public StateRelevance OnRightWall = StateRelevance.NotRelevant;
        public StateRelevance Scared = StateRelevance.NotRelevant;
        public StateRelevance WallSliding = StateRelevance.NotRelevant;
        public StateRelevance WallSlidingDown = StateRelevance.NotRelevant;
        public StateRelevance WallClimbing = StateRelevance.NotRelevant;
        public StateRelevance WallClamping = StateRelevance.NotRelevant;
        public StateRelevance Walking = StateRelevance.NotRelevant;
        public StateRelevance Interacting = StateRelevance.NotRelevant;

        public bool IsBlockedByGuard(CharacterState data)
        {
            if (CheckIfBlockedByGuard(data.Airborn, Airborn)) return true;
            if (CheckIfBlockedByGuard(data.Grounded, Grounded)) return true;
            if (CheckIfBlockedByGuard(data.Falling, Falling)) return true;
            if (CheckIfBlockedByGuard(data.Jumping, Jumping)) return true;
            if (CheckIfBlockedByGuard(data.OnWall, OnWall)) return true;
            if (CheckIfBlockedByGuard(data.OnLeftWall, OnLeftWall)) return true;
            if (CheckIfBlockedByGuard(data.OnRightWall, OnRightWall)) return true;
            if (CheckIfBlockedByGuard(data.Scared, Scared)) return true;
            if (CheckIfBlockedByGuard(data.WallSliding, WallSliding)) return true;
            if (CheckIfBlockedByGuard(data.WallClamping, WallClamping)) return true;
            if (CheckIfBlockedByGuard(data.WallSlidingDown, WallSlidingDown)) return true;
            if (CheckIfBlockedByGuard(data.WallClimbing, WallClimbing)) return true;
            if (CheckIfBlockedByGuard(data.Walking, Walking)) return true;
            if (CheckIfBlockedByGuard(data.Interacting, Interacting)) return true;
            
            return false;
        }

        private bool CheckIfBlockedByGuard(StateData data, StateRelevance relevance)
        {
            switch (relevance)
            {
                case StateRelevance.Required:
                    return !data.Currently;
                case StateRelevance.Blocked:
                    return data.Currently;
                default: return false;
            }
        }
    }

    public enum StateRelevance
    {
        NotRelevant,
        Blocked,
        Required
    }
}