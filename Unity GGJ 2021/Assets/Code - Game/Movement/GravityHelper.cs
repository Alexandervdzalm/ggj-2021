﻿using UnityEngine;

public class GravityHelper
{
    public static float GravityScale(float gravity)
    {
        return -gravity / Physics2D.gravity.y;
    }
}