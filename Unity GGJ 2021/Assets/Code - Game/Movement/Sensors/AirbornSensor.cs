using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirbornSensor : CombinedStateSensor
{
    private CharacterState state;
    
    void Start()
    {
        state = GetComponent<CharacterState>();
    }
    
    void FixedUpdate()
    {
        if (state.OnWall.Currently || state.Grounded.Currently)
        {
            state.Airborn.Stop();
        }
        else
        {
            state.Airborn.Start();
        }
    }
}
