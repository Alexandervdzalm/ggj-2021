using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private PlayerControls controls;
    private MovementController movementController;
    private GroundedJumpController groundedJumpController;
    private WallJumpController wallJumpController;
    private WallSlideController wallSlideController;

    void Start()
    {
        movementController = GetComponent<MovementController>();
        groundedJumpController = GetComponent<GroundedJumpController>();
        wallJumpController = GetComponent<WallJumpController>();
        wallSlideController = GetComponent<WallSlideController>();
    }
    void Awake()
    {
        controls = new PlayerControls();

        controls.Player.Move.performed += ctx => movementController.SetInput(ctx.ReadValue<Vector2>().x);
        controls.Player.Move.canceled += ctx => movementController.SetInput(0);

        controls.Player.Move.performed += ctx => wallSlideController.SetInput(ctx.ReadValue<Vector2>());
        controls.Player.Move.canceled += ctx => wallSlideController.SetInput(Vector2.zero);

        controls.Player.Jump.performed += ctx => groundedJumpController.StartJump();
        controls.Player.Jump.canceled += ctx => groundedJumpController.EndJump();
        
        controls.Player.Move.performed += ctx => wallJumpController.SetInput(ctx.ReadValue<Vector2>());
        controls.Player.Jump.performed += ctx => wallJumpController.StartJump();
        controls.Player.Jump.canceled += ctx => wallJumpController.EndJump();
    }

    private void OnEnable()
    {
        controls.Player.Enable();
    }
    private void OnDisable()
    {
        controls.Player.Disable();
    }
}
