﻿using UnityEngine;

public static class MovementHelper
{
    public static float CalculateNewVelocityAfterAcceleration(
        float maxSpeed,
        float timeToAccel,
        float timeToDeAccel,
        float input,
        float velocity,
        float movementTreshold = 0.3f,
        float inputDeadZone = 0.05f)
    {
        bool accelerating = System.Math.Abs(Mathf.Sign(input) - Mathf.Sign(velocity)) < 0.01f;
        bool stopped = Mathf.Abs(velocity) < movementTreshold;
        bool noInput = Mathf.Abs(input) < inputDeadZone;

        if (stopped && noInput)
        {
            return 0f;
        }
        
        if (accelerating && !noInput)
        {
            float accel = maxSpeed * (Time.fixedDeltaTime / timeToAccel);
            return Mathf.Clamp(velocity + input * accel, -maxSpeed, maxSpeed);
        }
        
        // Breaking
        float deAccel = Mathf.Sign(velocity) * maxSpeed * (Time.fixedDeltaTime / timeToDeAccel);
        return velocity - deAccel;
    }
}

public enum MovementState
{
    Accelerating,
    Breaking,
    Stopped
}