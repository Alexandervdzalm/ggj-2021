using Sirenix.OdinInspector;
using UnityEngine;

public class JumpTokens : MonoBehaviour
{
    [ShowInInspector]
    private int JumpTokenAmount = 1;

    public void ResetTokens()
    {
        JumpTokenAmount = SettingsHolder.Instance.settings.JumpTokenAmount;
    }

    public bool HasEnoughTokens()
    {
        return JumpTokenAmount > 0;
    }
    

    public bool CanJumpAndConsumeJumpToken()
    {
        if (HasEnoughTokens())
        {
            JumpTokenAmount--;
            return true;
        }

        return false;
    }
    
    
}