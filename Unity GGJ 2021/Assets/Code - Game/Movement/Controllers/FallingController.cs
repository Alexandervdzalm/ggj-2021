﻿using UnityEngine;

public class FallingController : MonoBehaviour
{
    private GameSettings settings;
    private Rigidbody2D rb;
    private CharacterState state;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
    }

    void FixedUpdate()
    {
        if (settings.FallingGuard.IsBlockedByGuard(state))
        {
            state.Falling.Stop();
            return;
        }

        if (rb.velocity.y < 0.01f )
        {
            state.Falling.Start();
            
            float gravity = 2 * settings.MaxJumpHeight / (settings.TimeToFall * settings.TimeToFall);
            rb.gravityScale = GravityHelper.GravityScale(gravity);
        }
        else
        {
            state.Falling.Stop();
            
            float gravity = 2 * settings.MaxJumpHeight / (settings.TimeToMaxJumpApex * settings.TimeToMaxJumpApex);
            rb.gravityScale = GravityHelper.GravityScale(gravity);
        }
    }
}