using Interactable;

using Sirenix.OdinInspector;

using UnityEngine;

class InventoryController : ObjectQueue
{
    [SerializeField]
    protected Vector2 dequeueOffset = Vector2.zero;
    [SerializeField]
    protected Vector2 dequeueVelocity = Vector2.zero;

    protected new void Awake()
    {
        base.Awake();

        onDequeue.AddListener(OnDequeue);
        onEnqueue.AddListener(OnEnqueue);
    }

    protected void OnDestroy()
    {
        onDequeue.RemoveListener(OnDequeue);
        onEnqueue.RemoveListener(OnEnqueue);
    }

    protected void OnDequeue(GameObject item)
    {
        Rigidbody2D rigidbody2D = item.GetComponent<Rigidbody2D>();

        // Set new position.
        item.transform.position = new Vector3(transform.position.x + dequeueOffset.x, transform.position.y + dequeueOffset.y);

        // Activate element.
        item.SetActive(true);

        // Set initial force.
        rigidbody2D.velocity = new Vector3(dequeueVelocity.x, dequeueVelocity.y, 0f);
    }

    protected void OnEnqueue(GameObject item)
    {
        item.SetActive(false);
    }

    public void Pop()
    {
        Dequeue();
    }

    public void Empty()
    {
        DequeueAll();
    }

    public bool HasItems()
    {
        return items.Count > 0;
    }
}
