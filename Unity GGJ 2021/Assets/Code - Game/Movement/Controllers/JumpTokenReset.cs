﻿using System;
using UnityEngine;

[RequireComponent(typeof(JumpTokens),typeof(CharacterState))]
public class JumpTokenReset : MonoBehaviour
{
    private JumpTokens jumpTokens;
    private CharacterState state;
    
    private void Start()
    {
        jumpTokens = GetComponent<JumpTokens>();
        state = GetComponent<CharacterState>();
        
        state.Grounded.OnStart.AddListener(() => jumpTokens.ResetTokens());
        state.OnWall.OnStop.AddListener(() => jumpTokens.ResetTokens());
    }
}