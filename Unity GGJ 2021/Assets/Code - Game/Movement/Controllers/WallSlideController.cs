using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Sirenix.OdinInspector;
using UnityEngine;

public class WallSlideController : MonoBehaviour
{
    private GameSettings settings;
    private Rigidbody2D rb;
    private CharacterState state;
    [ShowInInspector] private Vector2 input;

    private Vector2 velocity => rb.velocity;
    private float vx => velocity.x;
    private float vy => velocity.y;
    private float ix => input.x;
    private float iy => input.y;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
    }

    public void SetInput(Vector2 setInput)
    {
        this.input = setInput;
    }

    void FixedUpdate()
    {
        InitiateWallSlideFromGround();
        WallSlide();
    }

    private void InitiateWallSlideFromGround()
    {
        if (settings.WallSlideFromGroundTransitionGuard.IsBlockedByGuard(state))
            return;

        // Transition when input and velocity
        if (input.magnitude > 0 && velocity.magnitude > 0)
        {
            rb.velocity = new Vector2(0, vy + vx);
        }
    }

    private void WallSlide()
    {
        if (settings.WallSlideGuard.IsBlockedByGuard(state))
        {
            state.WallSliding.Stop();
            state.WallSlidingDown.Stop();
            state.WallClamping.Stop();
            state.WallClimbing.Stop();
            return;
        }

        state.WallSliding.Start();
        rb.gravityScale = 0.0f;
        
        bool climbing = vy >= -0.01f;
        bool wantToClimb = iy > settings.YDeadZone;
        var onLeftWall = state.OnLeftWall.Currently;
        var onRightWall = state.OnRightWall.Currently;
        float stickToTheWallVelocity = 0;
        stickToTheWallVelocity += onLeftWall ? -1.0f : 0f;
        stickToTheWallVelocity += onRightWall ? 1.0f : 0f;
        if (climbing || wantToClimb)
        {
            bool horizontalInToWallInput = onLeftWall && (ix < -.01f) ||
                                           onRightWall && (ix > 0.1f);
            float calculatedInput = horizontalInToWallInput 
                ? Mathf.Min(new Vector2(input.x * 0.5f, input.y).magnitude, 1f) * Mathf.Sign(iy)
                : iy;
            float newYVelocity = MovementHelper.CalculateNewVelocityAfterAcceleration(
                settings.SlideClimbTopSpeed,
                settings.SlideClimbTimeToMaxSpeed,
                settings.SlideClimbTimeToStopFromMaxSpeed,
                calculatedInput,
                vy,
                settings.SlideClimbMovementTreshold,
                settings.YDeadZone);
            rb.velocity = new Vector2(stickToTheWallVelocity, newYVelocity);
        }
        else
        {
            float newYVelocity = MovementHelper.CalculateNewVelocityAfterAcceleration(
                settings.SlideDownTopSpeed,
                settings.SlideDownTimeToMaxSpeed,
                settings.SlideDownTimeToStopFromMaxSpeed,
                iy,
                vy,
                settings.SlideClimbMovementTreshold,
                settings.YDeadZone);
            rb.velocity = new Vector2(stickToTheWallVelocity, newYVelocity);
        }

        if (state.Grounded.Currently && Mathf.Abs(input.y) < settings.YDeadZone)
        {
            // Grounded
            state.WallSlidingDown.Stop();
            state.WallClamping.Stop();
            state.WallClimbing.Stop();
        }
        else if (vy > settings.SlideClimbMovementTreshold)
        {
            //WallClimb
            state.WallSlidingDown.Stop();
            state.WallClamping.Stop();
            state.WallClimbing.Start();
        }
        else if (vy < -settings.SlideClimbMovementTreshold)
        {
            //WallSlideDown
            state.WallSlidingDown.Start();
            state.WallClamping.Stop();
            state.WallClimbing.Stop();
        }
        else
        {
            // WallClamping
            state.WallSlidingDown.Stop();
            state.WallClamping.Start();
            state.WallClimbing.Stop();
        }
    }
}