using System;
using System.Collections;

using UnityEngine;
using UnityEngine.Events;

public class GroundedJumpController : JumpController
{
    private Coroutine jumpCoroutine;

    public void StartJump()
    {
        StopAllCoroutines();
        jumpCoroutine = StartCoroutine(CheckJump());
    }

    private IEnumerator CheckJump()
    {
        double time = Time.timeSinceLevelLoadAsDouble;

        while (IsNonValidJumpState() && TimeLeftToLandForJumpQueue(time))
        {
            yield return null;
        }

        if (IsNonValidJumpState())
            yield break;

        if (!jumpTokens.CanJumpAndConsumeJumpToken())
            yield break;

        StartJumpBase(settings.TimeToMaxJumpApex, settings.MaxJumpHeight);

        if (jumpEndedPrematurely)
            EndJump();
    }

    private bool TimeLeftToLandForJumpQueue(double time)
    {
        return Time.timeSinceLevelLoadAsDouble - time < settings.JumpQueueTime;
    }

    private bool IsNonValidJumpState()
    {
        bool outsideCoyoteTime = state.LastGrounded.TimeOn > settings.CoyoteTime;
        return outsideCoyoteTime && settings.JumpGuard.IsBlockedByGuard(state);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
