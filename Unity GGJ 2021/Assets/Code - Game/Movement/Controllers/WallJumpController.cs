using System;
using UnityEngine;

public class WallJumpController : JumpController
{
    private Vector2 input = Vector2.zero;

    public void SetInput(Vector2 externalInput)
    {
        input = externalInput;
    }

    public void StartJump()
    {
        if (settings.WallSlideJumpGuard.IsBlockedByGuard(state))
        {
            return;
        }

        state.Jumping.Start();

        var horizontalMinimum = 0.2f;
        var verticalMinimum = .2f;

        var horizontalMinimumDir = (state.OnRightWall.Currently ? -1 : 1) * horizontalMinimum;
        float directionX = horizontalMinimumDir
                           + (1 - horizontalMinimum) * input.x;
        float directionY = verticalMinimum
                           + (1 - verticalMinimum) * input.y;
        
        // If player presses into the wall
        if (Math.Abs(Mathf.Sign(horizontalMinimumDir) - Mathf.Sign(input.x)) > 0.01f)
        {
            directionX = horizontalMinimumDir;
            if (Mathf.Abs(input.y) < 0.1f)
            {
                directionY = 1.0f;
            }
        }
        var jumpDirection = new Vector2(directionX, directionY);

        StartJumpDirectional(settings.TimeToMaxJumpApex, settings.MaxJumpHeight, jumpDirection);
    }
}