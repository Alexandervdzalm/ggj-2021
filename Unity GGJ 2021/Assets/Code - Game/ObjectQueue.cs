using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

class ObjectQueue : MonoBehaviour
{
    protected Queue<GameObject> items;

    public UnityEvent<GameObject> onDequeue;
    public UnityEvent<GameObject> onEnqueue;

    protected void Awake()
    {
        items = new Queue<GameObject>();
        Clear();
    }

    public void Clear()
    {
        items.Clear();
    }

    public GameObject Dequeue()
    {
        GameObject item = items.Dequeue();
        onDequeue.Invoke(item);
        return item;
    }

    public GameObject[] DequeueAll()
    {
        GameObject[] itemsDequeued = items.ToArray();
        items.Clear();

        foreach (GameObject _item in itemsDequeued)
        {
            onDequeue.Invoke(_item);
        }
        return itemsDequeued;
    }

    public void Enqueue(GameObject _item)
    {
        if (items.Contains(_item))
        {
            return;
        }

        items.Enqueue(_item);
        onEnqueue.Invoke(_item);
    }

    public void Enqueue(GameObject[] _items)
    {
        foreach (GameObject _item in _items)
        {
            items.Enqueue(_item);
            onEnqueue.Invoke(_item);
        }
    }

    public GameObject[] Get()
    {
        return items.ToArray();
    }

    public GameObject Peek()
    {
        return items.Peek();
    }
}
