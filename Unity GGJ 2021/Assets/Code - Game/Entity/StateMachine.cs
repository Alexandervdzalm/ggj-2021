using System.Collections.Generic;

using UnityEngine;

namespace Entity
{
    class StateMachine : MonoBehaviour
    {
        public bool autoRetrieve = false;
        public List<State> states = new List<State>();

        protected void Awake()
        {
            // Automatically retrieve the states and add it to the list.
            if (autoRetrieve) {
                State[] statesTemp = GetComponents<State>();
                foreach (State state in statesTemp)
                {
                    if (states.Contains(state)) {
                        continue;
                    }
                    states.Add(state);
                }
            }
        }

        protected void Update()
        {
            // Activate the first state that says it can be active, and disable all others.
            bool activatedState = false;
            foreach (State state in states)
            {
                if (!activatedState && state.Can()) {
                    activatedState = true;
                    state.enabled = true;
                    continue;
                }

                state.enabled = false;
            }
        }
    }
}