using UnityEngine;

namespace Utils {
    class DebugBehaviour : MonoBehaviour {
        public void Error(object message) {
            Debug.LogError(message);
        }

        public void Error(object message, Object context) {
            Debug.LogError(message, context);
        }

        public void Log(object message) {
            Debug.Log(message);
        }

        public void Log(object message, Object context) {
            Debug.Log(message, context);
        }

        public void Warn(object message) {
            Debug.LogWarning(message);
        }

        public void Warn(object message, Object context) {
            Debug.LogWarning(message, context);
        }
    }
}