using Sirenix.OdinInspector;
using UnityEngine;

public class TriggerEvent : SerializedMonoBehaviour
{
    public LayerMask ConditionalLayerMask;
    public BetterEvent OnTriggerEnter;
    public BetterEvent OnTriggerExit;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (MatchesLayerMask(other))
        {
            OnTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (MatchesLayerMask(other))
        {
            OnTriggerExit.Invoke();
        }
    }
    
    private bool MatchesLayerMask(Collider2D other)
    {
        return (ConditionalLayerMask & (1 << other.gameObject.layer)) > 0;
    }


}
