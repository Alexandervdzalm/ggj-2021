﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu]
[InlineEditor()]
public class MoveToTargetPidSettings : ScriptableObject
{
    public PIDControllerSettings XPidSettings, YPidSettings, ZPidSetttings;
}
